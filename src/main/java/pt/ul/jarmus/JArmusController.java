package pt.ul.jarmus;

import java.util.Collection;

public interface JArmusController {

	void start();

	void stop();

	/**
	 * Deregisters the given task from the resource.
	 * 
	 * @param thread
	 * @param synch
	 * @throws when the object is not regsitered with.
	 */
	void deregister(Object synch) throws IllegalStateException;

	/**
	 * Tries to deregister if possible.
	 * @param synch The object we are deregistering from.
	 * @return The success of the operation.
	 */
	boolean tryDeregister(Object synch);
	
	/**
	 * De-registers all objects tracked by the current thread.
	 */
	void deregisterAll();

	/**
	 * Returns wether or not the resource is registered with the given task.
	 * 
	 * @param task
	 * @param synch
	 * @return
	 */
	boolean isRegistered(Object synch);

	/**
	 * @param synch
	 * @throw Throws an exception when the given task is not registered with
	 *        <code>synch</code>.
	 */
	void ensureRegistered(Object synch) throws IllegalStateException;

	/**
	 * Register a task with a synchronization on a given phase.
	 * 
	 * @param task
	 * @param synch
	 * @param phase
	 */
	void register(Object synch, int phase);

	/**
	 * Advance the phase of a synchronization object the task is already
	 * registered with.
	 * 
	 * @param task
	 * @param synch
	 * @return
	 */
	int advance(Object synch);

	/**
	 * Returns the phase of the given synchronization object.
	 * @param synch
	 * @return
	 */
	int getPhase(Object synch);

	/**
	 * Increments the phase and awaits on it. Optimized code path for cyclic
	 * barriers.
	 * 
	 * @param synch
	 * @return
	 * @throws DeadlockIdentifiedException
	 */
	int beforeAdvanceAwait(Object synch)
			throws DeadlockIdentifiedException;

	/**
	 * Begins a new context. This should be called when the thread is executing
	 * a new task.
	 */
	void beforeTask();

	/**
	 * Finish the context. This should be called after the task executes a task.
	 */
	public abstract void afterTask();

	/**
	 * Await on many synchronization objects for a given phase.
	 * 
	 * @param task
	 * @param synch
	 * @param phase
	 */
	void beforeAwaitMany(Collection<?> synch, int phase)
			throws DeadlockIdentifiedException;

	/**
	 * Await on the synchronization object for a given phase.
	 * 
	 * @param task
	 * @param synch
	 * @param phase
	 */
	void beforeAwait(Object synch, int phase)
			throws DeadlockIdentifiedException;

	/**
	 * To be called after an await is issued.
	 */
	void afterAwait();

}