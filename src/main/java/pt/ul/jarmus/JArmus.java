package pt.ul.jarmus;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Phaser;

// The public API.

/**
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public class JArmus {
	private static final JArmusSetup DEFAULT = new JArmusSetup();
	/**
	 * Register any <code>CountDownLatch</code> that the current thread is going
	 * to issue a <code>countDown</code>.
	 * 
	 * @param countDownLatch
	 */
	public static void register(CountDownLatch countDownLatch) {
		DEFAULT.registerSynchronizer(countDownLatch, CountDownLatch.class);
	}

	/**
	 * Register any <code>Phaser</code> that the current thread is arriving at.
	 * 
	 * @param phaser
	 */
	public static void register(Phaser phaser) {
		DEFAULT.registerSynchronizer(phaser, Phaser.class);
	}

	/**
	 * Register any <code>CyclicBarrier</code> that the current thread is
	 * awaiting at.
	 * 
	 * @param cyclicBarrier
	 */
	public static void register(CyclicBarrier cyclicBarrier) {
		DEFAULT.registerSynchronizer(cyclicBarrier, CyclicBarrier.class);
	}
	
	/**
	 * Get the object that setups the internals of JArmus.
	 */
	public static JArmusSetup getSetup() {
		return DEFAULT;
	}
}
