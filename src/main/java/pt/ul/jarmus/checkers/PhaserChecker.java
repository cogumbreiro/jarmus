package pt.ul.jarmus.checkers;

import java.util.concurrent.Phaser;

import pt.ul.jarmus.JArmus;
import pt.ul.jarmus.JArmusController;

public class PhaserChecker implements PhaserListener, ResourceRegister<Phaser> {

	@Override
	public void register(Phaser phaser) {
		getController().register(phaser, phaser.getPhase());
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pt.ul.jarmus.checkers.PhaserListener#onArrive(java.util.concurrent.Phaser
	 * )
	 */
	@Override
	public void onArrive(Phaser phaser) {
		getController().advance(phaser);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pt.ul.jarmus.checkers.PhaserListener#beforeAwaitAdvance(java.util.concurrent
	 * .Phaser, int)
	 */
	@Override
	public void beforeAwaitAdvance(Phaser phaser, int phase) {
		getController().beforeAwait(phaser, phase);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pt.ul.jarmus.checkers.PhaserListener#afterAwaitAdvance()
	 */
	@Override
	public void afterAwaitAdvance() {
		getController().afterAwait();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pt.ul.jarmus.checkers.PhaserListener#beforeArriveAndAwaitAdvance(java
	 * .util.concurrent.Phaser)
	 */
	@Override
	public void beforeArriveAndAwaitAdvance(Phaser phaser) {
		getController().beforeAdvanceAwait(phaser);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pt.ul.jarmus.checkers.PhaserListener#afterArriveAndAwaitAdvance()
	 */
	@Override
	public void afterArriveAndAwaitAdvance() {
		getController().afterAwait();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pt.ul.jarmus.checkers.PhaserListener#onArriveAndDeregister(java.util.
	 * concurrent.Phaser)
	 */
	@Override
	public void onArriveAndDeregister(Phaser phaser) {
		getController().deregister(phaser);
	}

	/**
	 * Returns the default controller.
	 * @return
	 */
	private static JArmusController getController() {
		return JArmus.getSetup().getController();
	}
	
	public static final PhaserChecker DEFAULT = new PhaserChecker();
}
