package pt.ul.jarmus.checkers;

import java.util.concurrent.CyclicBarrier;
/**
 * Represents the events we are interested in.
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 *
 */
public interface CyclicBarrierListener {
	void beforeAwait(CyclicBarrier barrier);
	void afterAwait();
}
