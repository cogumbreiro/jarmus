package pt.ul.jarmus.checkers;

import java.util.concurrent.CountDownLatch;

public interface CountDownLatchListener {

	void onCountDown(CountDownLatch latch);

	void beforeAwait(CountDownLatch synch);

	void afterAwait();

}