package pt.ul.jarmus.checkers;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public interface ReentrantLockListener {
	/**
	 * Before the blocking call to {@link Lock#lock()}
	 * 
	 * @param task
	 */
	void beforeLock(ReentrantLock lock);

	/**
	 * After the blocking call to {@link Lock#lock()}
	 */
	void afterLock(ReentrantLock lock);

	/**
	 * Enters the critical region. This should called after a successful
	 * {@link Lock#lock()}.
	 * 
	 * @param lock
	 */
	void enterCritical(ReentrantLock lock);

	/**
	 * Called when invoking {@link Lock#unlock()}.
	 * 
	 * @param lock
	 */
	void beforeUnlock(ReentrantLock lock);
}