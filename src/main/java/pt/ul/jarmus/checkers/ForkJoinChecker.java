package pt.ul.jarmus.checkers;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.ForkJoinTask;

import pt.ul.jarmus.JArmus;
import pt.ul.jarmus.JArmusController;

public class ForkJoinChecker implements ForkJoinListener {
	
	@Override
	public void beforeInvokeAll(Collection<? extends ForkJoinTask<?>> tasks) {
		JArmusController handler = getController();
		handler.beforeAwaitMany(tasks, 1);
		handler.beforeTask();
	}
	
	@Override
	public void afterInvokeAll() {
		JArmusController handler = getController();
		handler.afterTask();
		handler.afterAwait();
	}
	
	@Override
	public void beforeInvoke(ForkJoinTask<?> task) {
		JArmusController handler = getController();
		handler.beforeAwait(task, 1);
		handler.beforeTask();
	}
	
	@Override
	public void afterInvoke() {
		JArmusController handler = getController();
		handler.afterTask();
		handler.afterAwait();
	}
	
	/**
	 * Before the blocking call
	 * @param task
	 */
	@Override
	public void beforeJoin(ForkJoinTask<?> task) {
		JArmusController handler = getController();
		handler.beforeAwait(task, 1);
	}
	/**
	 * After the blocking call
	 */
	@Override
	public void afterJoin() {
		JArmusController handler = getController();
		handler.afterAwait();
	}

	/**
	 * Registers the thread from the join
	 * @param task
	 */
	@Override
	public void beforeExec(ForkJoinTask<?> task) {
		JArmusController handler = getController();
		handler.register(task, 1);
	}

	/**
	 * Deregisters the thread from the join
	 * @param task
	 */
	@Override
	public void afterExec(ForkJoinTask<?> task) {
		JArmusController handler = getController();
		handler.deregister(task);
	}

	@Override
	public void beforeGet(ForkJoinTask<?> f) {
		beforeJoin(f);
	}

	@Override
	public void afterGet() {
		afterJoin();
	}

	@Override
	public void beforeInvokeAll(ForkJoinTask<?>... tasks) {
		JArmusController handler = getController();
		handler.beforeAwaitMany(Arrays.asList(tasks), 1);
		handler.beforeTask();
	}

	@Override
	public void beforeInvokeAll(ForkJoinTask<?> task1, ForkJoinTask<?> task2) {
		JArmusController handler = getController();
		handler.beforeAwaitMany(Arrays.asList(task1, task2), 1);
		handler.beforeTask();
	}

	private static JArmusController getController() {
		return JArmus.getSetup().getController();
	}
}
