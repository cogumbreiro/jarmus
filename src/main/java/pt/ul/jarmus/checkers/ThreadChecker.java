package pt.ul.jarmus.checkers;

import pt.ul.jarmus.JArmus;
import pt.ul.jarmus.JArmusController;

public class ThreadChecker implements ThreadListener {
	/**
	 * Before the blocking call
	 * @param task
	 */
	public void beforeJoin(Thread task) {
		getController().beforeAwait(task, 1);
	}
	/**
	 * After the blocking call
	 */
	public void afterJoin() {
		getController().afterAwait();
	}
	private static JArmusController getController() {
		return JArmus.getSetup().getController();
	}
}
