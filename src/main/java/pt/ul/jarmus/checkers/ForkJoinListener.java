package pt.ul.jarmus.checkers;

import java.util.Collection;
import java.util.concurrent.ForkJoinTask;

public interface ForkJoinListener {
	void beforeInvoke(ForkJoinTask<?> task);

	void afterInvoke();

	void beforeJoin(ForkJoinTask<?> task);

	void afterJoin();

	void beforeExec(ForkJoinTask<?> task);

	void afterExec(ForkJoinTask<?> task);

	void beforeInvokeAll(Collection<? extends ForkJoinTask<?>> tasks);

	void beforeInvokeAll(ForkJoinTask<?>... tasks);

	void beforeInvokeAll(ForkJoinTask<?> task1, ForkJoinTask<?> task2);
	
	void afterInvokeAll();

	void beforeGet(ForkJoinTask<?> f);

	void afterGet();
}