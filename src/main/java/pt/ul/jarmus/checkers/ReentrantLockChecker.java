package pt.ul.jarmus.checkers;

import java.util.concurrent.locks.ReentrantLock;

import pt.ul.jarmus.JArmus;
import pt.ul.jarmus.JArmusController;

public class ReentrantLockChecker implements ReentrantLockListener, ResourceRegister<ReentrantLock> {

	private static JArmusController getController() {
		return JArmus.getSetup().getController();
	}

	@Override
	public void beforeUnlock(ReentrantLock lock) {
		if (lock.getHoldCount() == 1) {
			getController().tryDeregister(lock);
		}
	}

	@Override
	public void beforeLock(ReentrantLock lock) {
		if (!lock.isHeldByCurrentThread()) {
			getController().beforeAwait(lock, 0);
		}
	}

	@Override
	public void afterLock(ReentrantLock lock) {
		if (lock.getHoldCount() == 1) {
			getController().afterAwait();
		}
	}
	
	@Override
	public void enterCritical(ReentrantLock lock) {
		if (lock.getHoldCount() == 1) {
			getController().register(lock, 0);
		}
	}

	@Override
	public void register(ReentrantLock lock) {
		getController().register(lock, 0);
	}
	
	public static final ReentrantLockChecker DEFAULT = new ReentrantLockChecker();
}
