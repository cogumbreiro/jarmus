package pt.ul.jarmus.checkers;


public interface ResourceRegister<T> {
	void register(T synch);
}
