package pt.ul.jarmus.checkers;

import java.util.concurrent.CountDownLatch;

import pt.ul.jarmus.JArmus;
import pt.ul.jarmus.JArmusController;

public class CountDownLatchChecker implements CountDownLatchListener,
		ResourceRegister<CountDownLatch> {
	private static JArmusController getController() {
		return JArmus.getSetup().getController();
	}

	public void register(CountDownLatch countDownLatch) {
		getController().register(countDownLatch, 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pt.ul.jarmus.checkers.CountDownLatchListener#onCountDown(java.util.concurrent
	 * .CountDownLatch)
	 */
	@Override
	public void onCountDown(CountDownLatch latch) {
		getController().advance(latch);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pt.ul.jarmus.checkers.CountDownLatchListener#beforeAwait(java.util.concurrent
	 * .CountDownLatch)
	 */
	@Override
	public void beforeAwait(CountDownLatch synch) {
		getController().beforeAwait(synch, 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pt.ul.jarmus.checkers.CountDownLatchListener#afterAwait()
	 */
	@Override
	public void afterAwait() {
		getController().afterAwait();
	}
	
	public static final CountDownLatchChecker DEFAULT = new CountDownLatchChecker();
}
