package pt.ul.jarmus.checkers;

public interface ThreadListener {
	/**
	 * Before the blocking call
	 * @param task
	 */
	void beforeJoin(Thread task);
	/**
	 * After the blocking call
	 */
	void afterJoin();
}