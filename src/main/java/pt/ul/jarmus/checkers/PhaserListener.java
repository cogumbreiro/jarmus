package pt.ul.jarmus.checkers;

import java.util.concurrent.Phaser;

public interface PhaserListener {

	void onArrive(Phaser phaser);

	void beforeAwaitAdvance(Phaser phaser, int phase);

	void afterAwaitAdvance();

	void beforeArriveAndAwaitAdvance(Phaser phaser);

	void afterArriveAndAwaitAdvance();

	void onArriveAndDeregister(Phaser phaser);

}