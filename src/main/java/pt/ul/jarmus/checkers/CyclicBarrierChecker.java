package pt.ul.jarmus.checkers;

import java.util.concurrent.CyclicBarrier;

import pt.ul.jarmus.JArmus;
import pt.ul.jarmus.JArmusController;

public class CyclicBarrierChecker implements ResourceRegister<CyclicBarrier>, CyclicBarrierListener {
	public void register(CyclicBarrier barrier) {
		getController().register(barrier, 1);
	}

	public void beforeAwait(CyclicBarrier barrier) {
		JArmusController controller = getController();
		int phase = controller.advance(barrier);
		controller.beforeAwait(barrier, phase);
	}

	public void afterAwait() {
		getController().afterAwait();
	}

	private static JArmusController getController() {
		return JArmus.getSetup().getController();
	}
	
	public static final CyclicBarrierChecker DEFAULT = new CyclicBarrierChecker();
}
