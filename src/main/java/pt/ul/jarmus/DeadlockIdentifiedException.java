package pt.ul.jarmus;

import pt.ul.armus.DeadlockInfo;

/**
 * A deadlock was identified.
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 *
 */
public class DeadlockIdentifiedException extends RuntimeException {
	private static final long serialVersionUID = -681741062007033399L;
	private DeadlockInfo deadlock;

	
	public DeadlockIdentifiedException(DeadlockInfo deadlock) {
		super();
		this.deadlock = deadlock;
	}

	public DeadlockIdentifiedException(DeadlockInfo deadlock, String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.deadlock = deadlock;
	}

	public DeadlockIdentifiedException(DeadlockInfo deadlock, String message, Throwable cause) {
		super(message, cause);
		this.deadlock = deadlock;
	}

	public DeadlockIdentifiedException(DeadlockInfo deadlock, String message) {
		super(message);
		this.deadlock = deadlock;
	}

	public DeadlockIdentifiedException(DeadlockInfo deadlock, Throwable cause) {
		super(cause);
		this.deadlock = deadlock;
	}
	
	public DeadlockInfo getDeadlock() {
		return deadlock;
	}

	@Override
	public String toString() {
		return "DeadlockIdentifiedException [deadlock=" + deadlock + "]";
	}
}
