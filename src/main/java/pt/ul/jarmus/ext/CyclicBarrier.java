package pt.ul.jarmus.ext;

import java.util.concurrent.BrokenBarrierException;

import pt.ul.jarmus.checkers.CyclicBarrierChecker;
import pt.ul.jarmus.checkers.CyclicBarrierListener;

public class CyclicBarrier extends java.util.concurrent.CyclicBarrier {
	private CyclicBarrierListener listener = new CyclicBarrierChecker();

	public CyclicBarrier(int parties, Runnable barrierAction) {
		super(parties, barrierAction);
	}

	public CyclicBarrier(int parties) {
		super(parties);
	}

	public CyclicBarrierListener getListener() {
		return listener;
	}

	public void setListener(CyclicBarrierListener listener) {
		this.listener = listener;
	}

	@Override
	public int await() throws InterruptedException, BrokenBarrierException {
		listener.beforeAwait(this);
		try {
			return super.await();
		} finally {
			listener.afterAwait();
		}
	}

}
