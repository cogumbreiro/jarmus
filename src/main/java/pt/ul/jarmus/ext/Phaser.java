package pt.ul.jarmus.ext;

import pt.ul.jarmus.checkers.PhaserChecker;
import pt.ul.jarmus.checkers.PhaserListener;

public class Phaser extends java.util.concurrent.Phaser {
	private PhaserListener listener = new PhaserChecker();

	public PhaserListener getListener() {
		return listener;
	}

	public void setListener(PhaserListener listener) {
		this.listener = listener;
	}

	public Phaser() {
		super();
	}

	public Phaser(int parties) {
		super(parties);
	}

	public Phaser(java.util.concurrent.Phaser parent, int parties) {
		super(parent, parties);
	}

	public Phaser(java.util.concurrent.Phaser parent) {
		super(parent);
	}

	@Override
	public int arrive() {
		listener.onArrive(this);
		return super.arrive();
	}

	@Override
	public int arriveAndAwaitAdvance() {
		listener.beforeArriveAndAwaitAdvance(this);
		try {
			return super.arriveAndAwaitAdvance();
		} finally {
			listener.afterArriveAndAwaitAdvance();
		}
	}

	@Override
	public int awaitAdvance(int phase) {
		listener.beforeAwaitAdvance(this, phase);
		try {
			return super.awaitAdvance(phase);
		} finally {
			listener.afterAwaitAdvance();
		}
	}

	@Override
	public int arriveAndDeregister() {
		listener.onArriveAndDeregister(this);
		return super.arriveAndDeregister();
	}

	@Override
	public int awaitAdvanceInterruptibly(int phase) throws InterruptedException {
		listener.beforeAwaitAdvance(this, phase);
		try {
			return super.awaitAdvanceInterruptibly(phase);
		} finally {
			listener.afterAwaitAdvance();
		}
	}
}
