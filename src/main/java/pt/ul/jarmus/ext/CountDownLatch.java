package pt.ul.jarmus.ext;

import pt.ul.jarmus.checkers.CountDownLatchListener;

public class CountDownLatch extends java.util.concurrent.CountDownLatch {
	private CountDownLatchListener listener;

	public CountDownLatchListener getListener() {
		return listener;
	}

	public void setListener(CountDownLatchListener listener) {
		this.listener = listener;
	}

	public CountDownLatch(int count) {
		super(count);
	}

	@Override
	public void countDown() {
		listener.onCountDown(this);
		super.countDown();
	}

	@Override
	public void await() throws InterruptedException {
		listener.beforeAwait(this);
		try {
			super.await();
		} finally {
			listener.afterAwait();
		}
	}
}
