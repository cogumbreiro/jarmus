package pt.ul.jarmus;

import pt.ul.armus.Resource;

/**
 * Represents a specific synchronization phase of a resource.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@di.fc.ul.pt)
 * 
 */
public class ResourceVariable implements Resource {

	private final Object synch;
	private final int phase;
	private final int hashCode;

	public ResourceVariable(final Object synch, final int currPhase) {
		assert synch != null;
		assert currPhase >= 0;
		this.synch = synch;
		this.phase = currPhase;
		final int prime = 31;
		int result = 1;
		result = prime * result + phase;
		result = prime * result;
		this.hashCode = result;
	}

	public Object getSynch() {
		return synch;
	}

	public int getPhase() {
		return phase;
	}

	@Override
	public int hashCode() {
		return hashCode + System.identityHashCode(synch);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResourceVariable other = (ResourceVariable) obj;
		if (phase != other.phase)
			return false;
		if (synch == null) {
			if (other.synch != null)
				return false;
		} else if (!synch.equals(other.synch))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Resource[synch=" + synch + ", phase=" + phase + "]";
	}

}
