package pt.ul.jarmus;

import static pt.ul.jarmus.ExecUtil.joinPath;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import pt.ul.armus.conf.DetectionConfiguration;
import pt.ul.armus.conf.EdgeBufferConfiguration;
import pt.ul.armus.conf.GraphStrategy;
import pt.ul.armus.conf.MainConfiguration;
import pt.ul.armus.cycledetector.jgraph.JGraphTSolver;
import pt.ul.armus.deadlockresolver.FatalDeadlockResolver;
import pt.ul.armus.edgebuffer.EdgeBufferFactory;
import pt.ul.armus.edgebuffer.HashEdgeBufferFactory;
import pt.ul.armus.edgebuffer.QueueEdgeBufferFactory;

public class Harness {
	public static final String MAIN_DIR = joinPath("target", "main-classes");
	public static final String INST_DIR = joinPath("target", "inst-classes");
	public static final String INJ_DIR = joinPath("target", "inj-classes");
	private static final String POS_SAMPLES_DIR = joinPath("src", "pos-samples", "java");
	private static final String NEG_SAMPLES_DIR = joinPath("src", "neg-samples", "java");
	
	/**
	 * Returns the java builder with the default settings to use JArmus.
	 * @param className
	 * @return
	 */
	public static JavaProcessBuilder java(String className) {
		JavaProcessBuilder java = new JavaProcessBuilder(className);
		
		java.appendClasspath("lib/hppc.jar");
		java.appendClasspath("lib/jgrapht.jar");
		java.appendClasspath("lib/armus.jar");
		java.appendClasspath("lib/aspectjrt.jar");
		java.appendClasspath("target/main-classes/");
		java.appendClasspath("target/inst-aj-classes/");
		java.appendClasspath("target/examples-classes.jar");
		
		java.appendClasspath("target/pos-samples.jar");
		java.appendClasspath("target/neg-samples.jar");
		return java;
	}
	
	/**
	 * Get the class files in a directory
	 * @param directory
	 * @return
	 */
	private static Collection<String> getClasses(String directory) {
		List<String> result = new ArrayList<>();
	    for (String filename : new File(directory).list()) {
	    	String ext = ".java";
            if (filename.endsWith(ext)) {
            	result.add(filename.substring(0, filename.length() - ext.length()));
            }
	    }
	    Collections.sort(result);
	    return result;
	}
	
	public String getCurrentPackage() {
		String className = getClass().getName();
		int endIndex = className.lastIndexOf('.');
		if (endIndex == -1) {
			return null;
		} else {
			return className.substring(0, endIndex);
		}
		
	}
	
	private static String getPositiveTestsDir() {
		return POS_SAMPLES_DIR;
	}
	
	private static String getNegativeTestsDir() {
		return NEG_SAMPLES_DIR;
	}

	public static Collection<String> getPositiveTestNames() throws IOException {
		return getClasses(getPositiveTestsDir());
	}
	
	public static Collection<String> getNegativeTestNames() throws IOException {
		return getClasses(getNegativeTestsDir());
	}
	
	private static EdgeBufferConfiguration[] getBufferConf() {
		EdgeBufferFactory[] buffers = { new HashEdgeBufferFactory(),
				new QueueEdgeBufferFactory() };
		EdgeBufferConfiguration[] conf = new EdgeBufferConfiguration[buffers.length];
		int index = 0;
		for (EdgeBufferFactory fact : buffers) {
			conf[index] = new EdgeBufferConfiguration(true, fact);
			index++;
		}
		return conf;
	}

	public static Collection<MainConfiguration> getConfigurations() {
		ArrayList<MainConfiguration> result = new ArrayList<>();
		boolean[] trueFalse = new boolean[] { true, false };
		for (GraphStrategy graph : GraphStrategy.values()) {
			for (boolean avoidanceEnabled : trueFalse) {
				boolean detectionEnabled = !avoidanceEnabled;
				for (EdgeBufferConfiguration eConf : getBufferConf()) {
					DetectionConfiguration dConf = new DetectionConfiguration(
							detectionEnabled, 0, 1);
					JGraphTSolver detector = new JGraphTSolver();
					FatalDeadlockResolver resolver = new FatalDeadlockResolver();
					MainConfiguration conf = new MainConfiguration(dConf, eConf,
							avoidanceEnabled, detector, resolver, graph, false);
					result.add(conf);
				}
			}
		}
		return result;
	}
}
