package pt.ul.jarmus.checkers;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.concurrent.Phaser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import pt.ul.jarmus.JArmus;
import pt.ul.jarmus.JArmusController;

public class PhaserCheckerTest {
	JArmusController ctl;
	InOrder inOrder;
	PhaserChecker checker;
	Phaser b;

	@Before
	public void setUp() {
		ctl = mock(JArmusController.class);
		JArmus.getSetup().setController(ctl);
		inOrder = inOrder(ctl);
		checker = new PhaserChecker();
		b = new Phaser(1);
		inOrder.verify(ctl).start();
	}
	
	private JArmusController verifyCtl() {
		return inOrder.verify(ctl);		
	}
	
	@After
	public void after() {
		verifyNoMoreInteractions(ctl);
	}
	
	@Test
	public void testRegister() {
		checker.register(b);
		// check:
		verifyCtl().register(b, 0);
	}

	@Test
	public void testOnArrive() {
		checker.onArrive(b);
		// check:
		verifyCtl().advance(b);
	}

	@Test
	public void testBeforeAwaitAdvance() {
		checker.beforeAwaitAdvance(b, 0);
		// check:
		verifyCtl().beforeAwait(b, 0);
	}

	@Test
	public void testAfterAwaitAdvance() {
		checker.afterAwaitAdvance();
		// check:
		verifyCtl().afterAwait();
	}
	
	@Test
	public void testBeforeArriveAndAwaitAdvance() {
		checker.beforeArriveAndAwaitAdvance(b);
		// check:
		verifyCtl().beforeAdvanceAwait(b);
	}

	@Test
	public void testAfterArriveAndAwaitAdvance() {
		checker.afterArriveAndAwaitAdvance();
		// check:
		verifyCtl().afterAwait();
	}

	@Test
	public void testOnArriveAndDeregister() {
		checker.onArriveAndDeregister(b);
		// check:
		verifyCtl().deregister(b);
	}

}
