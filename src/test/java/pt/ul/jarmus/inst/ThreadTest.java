package pt.ul.jarmus.inst;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import pt.ul.jarmus.checkers.ThreadListener;
import pt.ul.jarmus.inst.ThreadObserver;

public class ThreadTest {
	ThreadListener listener;
	InOrder inOrder;

	@Before
	public void setUp() throws InstantiationException, IllegalAccessException {
		listener = Mockito.mock(ThreadListener.class);
		inOrder = inOrder(listener);
		ThreadObserver.aspectOf().setListener(listener);
	}

	@After
	public void checkNoMoreInteractions() {
		verifyNoMoreInteractions(listener);
	}

	@Test
	public void threadJoin() throws InterruptedException {
		final Thread th = new Thread();
		th.join();
		inOrder.verify(listener).beforeJoin(th);
		inOrder.verify(listener).afterJoin();
	}
}
