package pt.ul.jarmus;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import pt.ul.armus.conf.ConfigurationLoader;
import pt.ul.armus.conf.MainConfiguration;

/**
 * Runs all tests in pt.ul.jarmus.deadlocks under each verification algorithm.
 *
 */
@RunWith(value = Parameterized.class)
public class PositiveDeadlocksTest {
	private static final int TIMEOUT = 1000; // Milliseconds
	private String className;
	private MainConfiguration opts;
	public PositiveDeadlocksTest(String className, MainConfiguration options) {
		this.className = className;
		this.opts = options;
	}

	@Parameters(name = "{0} {1}")
	public static Collection<Object[]> data() throws IOException {
		List<Object[]> result = new ArrayList<>();
		for (String file : Harness.getPositiveTestNames()) {
			for (MainConfiguration conf : Harness.getConfigurations()) {
				result.add(new Object[] { file, conf });
			}
		}
		return result;
	}

	@Test
	public void test() throws IOException, InterruptedException, ExecutionException {
		JavaProcessBuilder java = Harness.java(className);
		java.setSystemProperties(ConfigurationLoader.render(opts));
		String[] result = ExecUtil.execUntil(TIMEOUT, java.build());
		String stderr = result[1].toLowerCase();
		String message = "No deadlock found stdout+stderr\n"; 
		message += java + "\n"; 
		message += stderr;
		assertTrue(message, stderr.contains("deadlock"));
	}

}
