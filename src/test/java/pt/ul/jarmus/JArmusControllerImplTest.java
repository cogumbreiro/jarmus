package pt.ul.jarmus;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pt.ul.armus.Armus;
import pt.ul.armus.edgebuffer.EdgeBuffer;
import pt.ul.armus.edgebuffer.TaskHandle;
import static org.mockito.Mockito.*;

public class JArmusControllerImplTest {
	JArmusController ver;
	Armus strat;

	@Before
	public void setUp() {
		strat = mock(Armus.class);
		ver = new JArmusControllerImpl(strat);		
	}

	@Test
	public void threadRegisteredPhase1() throws InstantiationException, IllegalAccessException {
		TaskHandle task = mock(TaskHandle.class);
		EdgeBuffer buffer = mock(EdgeBuffer.class);
		when(strat.getBuffer()).thenReturn(buffer);
		when(buffer.createHandle()).thenReturn(task);
		assertEquals(1, ver.getPhase(Thread.currentThread()));
	}

	@Test
	public void ensureThreadIsRegistered() throws InstantiationException, IllegalAccessException {
		TaskHandle task = mock(TaskHandle.class);
		EdgeBuffer buffer = mock(EdgeBuffer.class);
		when(strat.getBuffer()).thenReturn(buffer);
		when(buffer.createHandle()).thenReturn(task);
		ver.ensureRegistered(Thread.currentThread());
	}

	@Test
	public void testThreadIsRegistered() throws InstantiationException, IllegalAccessException {
		TaskHandle task = mock(TaskHandle.class);
		EdgeBuffer buffer = mock(EdgeBuffer.class);
		when(strat.getBuffer()).thenReturn(buffer);
		when(buffer.createHandle()).thenReturn(task);
		assertTrue(ver.isRegistered(Thread.currentThread()));
	}
	
	@Test
	public void testThreadIsRegisteredBeginTask() throws InstantiationException, IllegalAccessException {
		TaskHandle task = mock(TaskHandle.class);
		EdgeBuffer buffer = mock(EdgeBuffer.class);
		when(strat.getBuffer()).thenReturn(buffer);
		when(buffer.createHandle()).thenReturn(task);
		
		ver.beforeTask();
		assertTrue(ver.isRegistered(Thread.currentThread()));
		ver.afterTask();
		assertTrue(ver.isRegistered(Thread.currentThread()));
	}
	
}
