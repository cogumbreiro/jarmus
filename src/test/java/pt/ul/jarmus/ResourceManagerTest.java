package pt.ul.jarmus;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

import org.junit.Test;

import pt.ul.armus.edgebuffer.TaskHandle;
public class ResourceManagerTest {
	private static final Object synch = new Object();
	
	@Test
	public void testIsRegistered() {
		TaskHandle handle = mock(TaskHandle.class);
		ResourceManager m = new ResourceManager(handle);
		m.register(synch, 0);
		assertTrue(m.isRegistered(synch));
	}

	@Test
	public void testDeregister() {
		TaskHandle handle = mock(TaskHandle.class);
		ResourceManager m = new ResourceManager(handle);
		m.register(synch, 0);
		m.deregister(synch);
		assertFalse(m.isRegistered(synch));
	}

	@Test
	public void advanceRegistered() {
		TaskHandle handle = mock(TaskHandle.class);
		ResourceManager m = new ResourceManager(handle);
		m.register(synch, 0);
		assertEquals(0, m.advance(synch));
	}

	@Test
	public void advanceUnregistered() {
		TaskHandle handle = mock(TaskHandle.class);
		ResourceManager m = new ResourceManager(handle);
		try {
			m.advance(synch);
			fail("Should throw an exception.");
		} catch (IllegalStateException e) {
			// OK
		}
	}

	@Test
	public void ensureRegisteredOk() {
		TaskHandle handle = mock(TaskHandle.class);
		ResourceManager m = new ResourceManager(handle);
		m.register(synch, 0);
		m.ensureRegistered(synch);
	}

	@Test
	public void ensureRegisteredFail() {
		TaskHandle handle = mock(TaskHandle.class);
		ResourceManager m = new ResourceManager(handle);
		try {
			m.ensureRegistered(synch);
			fail("should throw an excaption");
		} catch (IllegalStateException e) {
			// ok
		}
	}

	@Test
	public void testClear() {
		TaskHandle handle = mock(TaskHandle.class);
		ResourceManager m = new ResourceManager(handle);
		m.register(synch, 0);
		m.clear();
		assertFalse(m.isRegistered(synch));
	}

}
