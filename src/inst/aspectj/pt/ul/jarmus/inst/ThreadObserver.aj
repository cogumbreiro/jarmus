package pt.ul.jarmus.inst;

import pt.ul.jarmus.checkers.ThreadChecker;
import pt.ul.jarmus.checkers.ThreadListener;

public aspect ThreadObserver {
	pointcut onJoin(Thread th): call (public void join()) && target(th);

	private ThreadListener listener = new ThreadChecker();

	public ThreadListener getListener() {
		return listener;
	}

	public void setListener(ThreadListener listener) {
		this.listener = listener;
	}

	void around(Thread th): onJoin(th) {
		listener.beforeJoin(th);
		try {
			proceed(th);
		} finally {
			listener.afterJoin();
		}
	}
}
