package pt.ul.jarmus.inst;

import java.util.concurrent.locks.ReentrantLock;

import pt.ul.jarmus.checkers.ReentrantLockListener;


public aspect ReentrantLockObserver {

	private ReentrantLockListener listener;
	
	public ReentrantLockListener getListener() {
		return listener;
	}
	
	public void setListener(ReentrantLockListener listener) {
		this.listener = listener;
	}
	
	pointcut onLock(ReentrantLock lck):
		call(void lock()) && target(lck);
	
	pointcut onUnlock(ReentrantLock lck):
		call(void unlock()) && target(lck);
	
	void around(ReentrantLock lck): onLock(lck) {
		listener.beforeLock(lck);
		try {
			proceed(lck);
			listener.enterCritical(lck);
		} finally {
			listener.afterLock(lck);
		}
	}

	before(ReentrantLock lck): onUnlock(lck) {
		listener.beforeUnlock(lck);
	}
}
