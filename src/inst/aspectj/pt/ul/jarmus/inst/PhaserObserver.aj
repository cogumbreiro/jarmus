package pt.ul.jarmus.inst;

import java.util.concurrent.Phaser;

import pt.ul.jarmus.checkers.PhaserChecker;
import pt.ul.jarmus.checkers.PhaserListener;

public aspect PhaserObserver {
	private PhaserListener listener = new PhaserChecker();

	pointcut onArrive(Phaser ph):
		call (public int arrive()) &&
		target(ph) && !target(pt.ul.jarmus.ext.Phaser);

	pointcut onAwaitAdvance(Phaser ph, int phase):
		call (public int awaitAdvance(int)) &&
		target(ph) && !target(pt.ul.jarmus.ext.Phaser) &&
		args(phase);

	pointcut onArriveAndAwaitAdvance(Phaser ph):
		call (public int arriveAndAwaitAdvance()) &&
		target(ph) && !target(pt.ul.jarmus.ext.Phaser);

	pointcut onArriveAndDeregister(Phaser ph):
		call (public int arriveAndDeregister()) &&
		target(ph) && !target(pt.ul.jarmus.ext.Phaser);

	before(Phaser ph): onArrive(ph) {
		listener.onArrive(ph);
	}

	int around(Phaser ph, int phase): onAwaitAdvance(ph, phase) {
		listener.beforeAwaitAdvance(ph, phase);
		try {
			return proceed(ph, phase);
		} finally {
			listener.afterAwaitAdvance();
		}
	}
	
	int around(Phaser ph): onArriveAndAwaitAdvance(ph) {
		listener.beforeArriveAndAwaitAdvance(ph);
		try {
			return proceed(ph);
		} finally {
			listener.afterArriveAndAwaitAdvance();
		}
	}

	before(Phaser ph): onArriveAndDeregister(ph) {
		listener.onArriveAndDeregister(ph);
	}

	public void setListener(PhaserListener listener) {
		this.listener = listener;
	}

	public PhaserListener getListenenr() {
		return listener;
	}
}
