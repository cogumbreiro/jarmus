package pt.ul.jarmus.inst;

import java.util.concurrent.CountDownLatch;

import pt.ul.jarmus.checkers.CountDownLatchChecker;
import pt.ul.jarmus.checkers.CountDownLatchListener;

public aspect CountDownLatchObserver {
	private CountDownLatchListener listener = new CountDownLatchChecker();
	
	public CountDownLatchListener getListener() {
		return listener;
	}

	public void setListener(CountDownLatchListener listener) {
		this.listener = listener;
	}

	pointcut onAwait(CountDownLatch b):
		call (public void await()) && 
		target(b) && !target(pt.ul.jarmus.ext.CountDownLatch);
	
	pointcut onCountDown(CountDownLatch b):
		call (public void countDown()) && 
		target(b) && !target(pt.ul.jarmus.ext.CountDownLatch);
	
	void around(CountDownLatch latch): onAwait(latch) {
		listener.beforeAwait(latch);
		try {
			proceed(latch);
		} finally {
			listener.afterAwait();
		}
	}
	
	before(CountDownLatch latch): onCountDown(latch) {
		listener.onCountDown(latch);
	}
}
