package pt.ul.jarmus.inst;

import java.util.concurrent.CyclicBarrier;

import pt.ul.jarmus.checkers.CyclicBarrierChecker;
import pt.ul.jarmus.checkers.CyclicBarrierListener;

public aspect CyclicBarrierObserver {
	private CyclicBarrierListener listener = new CyclicBarrierChecker();
	
	public CyclicBarrierListener getListener() {
		return listener;
	}

	public void setListener(CyclicBarrierListener listener) {
		this.listener = listener;
	}

	pointcut onAwait(CyclicBarrier b):
		call (public int await()) &&
		target(b) && !target(pt.ul.jarmus.ext.CyclicBarrier);

	int around(CyclicBarrier b): onAwait(b) {
		listener.beforeAwait(b);
		try {
			return proceed(b);
		} finally {
			listener.afterAwait();
		}
	}
}
