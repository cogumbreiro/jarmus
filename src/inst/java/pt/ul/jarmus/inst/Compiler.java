package pt.ul.jarmus.inst;


public class Compiler {
	private static final String ASPECTS_JAR = "aspects.jar";

	public static void main(String[] args) {
		if (args.length != 2) {
			System.err.println("Usage: jarmusc <INTPUT> <OUTPUT>");
			System.err.println("\tINPUT\t\tdirectory holding the compiled code");
			System.err.println("\tOUTPUT\t\tthe output location (directory or JAR)");
			System.exit(-1);
		}
		new Instrumenter(Compiler.class, ASPECTS_JAR).compile(args[0], args[1]);
	}
}
