package pt.ul.jarmus.inst;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.aspectj.tools.ajc.Main;
import org.zeroturnaround.zip.ZipUtil;

/**
 * Instruments code using some AspectJ aspects.
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 *
 */
public class Instrumenter {
	/**
	 * The relative path of the aspects jar.
	 */
	private final String aspectsJar;
	/**
	 * The relative path of the extras jar.
	 */
	private final String rtJar;
	/**
	 * A class that holds the resources (where the jars are located).
	 */
	private final Class<?> cls;

	/**
	 * @param cls
	 *            The class from which we take the resources.
	 * 
	 * @param aspectsJar
	 *            The path of the resource. A JAR with the compiled aspects that
	 *            will instrument the code.
	 * @param rtJar
	 *            The path of the resource, optional (null).
	 *            A JAR with some code to be injected in the outcome JAR.
	 */
	public Instrumenter(Class<?> cls, String aspectsJar, String rtJar) {
		this.cls = cls;
		this.aspectsJar = aspectsJar;
		this.rtJar = rtJar;
	}

	/**
	 * @param cls
	 *            The class from which we take the resources.
	 * 
	 * @param aspectsJar
	 *            The path of the resource. A JAR with the compiled aspects that
	 *            will instrument the code.
	 */
	public Instrumenter(Class<?> cls, String aspectsJar) {
		this(cls, aspectsJar, null);
	}

	/**
	 * 
	 * @param inputJarOrDir
	 *            An input directory or JAR containing the classes to be
	 *            instrumented.
	 * @param outputJar
	 */
	public void compile(String inputJarOrDir, String outputJar) {
		try {
			File aspects = copyResourceToTemporaryFile(cls, "/" + aspectsJar);
			File tmpDir = createTemporaryDirectory();
			List<String> args = new ArrayList<>(Arrays.asList(
					"-noExit", // VERY IMPORTANT! Otherwise, a System.exit() is issued.
					"-nowarn",
					"-showWeaveInfo",
					"-inpath", inputJarOrDir,
					"-aspectpath",aspects.toString()));
			if (rtJar != null) {
				args.add("-d");
				args.add(tmpDir.toString());
			} else {
				args.add("-outjar");
				args.add(outputJar);
			}
			try {
				Main.main(args.toArray(new String[0]));
				if (rtJar != null) {
					unzipResourceTo(Instrumenter.class, "/" + rtJar, tmpDir);
					File out = new File(outputJar);
					ZipUtil.pack(tmpDir, out);
				}
			} finally {
				FileUtils.deleteQuietly(tmpDir);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void unzipResourceTo(Class<?> cls, String resourceFilename, File output) throws IOException {
		File zip = copyResourceToTemporaryFile(cls, resourceFilename);
		try {
			ZipUtil.unpack(zip, output);
		} finally {
			FileUtils.deleteQuietly(zip);
		}
	}

	private static File createTemporaryDirectory() throws IOException {
		return Files.createTempDirectory(null).toFile();
	}

	private static File copyResourceToTemporaryFile(Class<?> cls, String resourceFilename) throws IOException {
		String suffix = FilenameUtils.getExtension(resourceFilename);
		File result = File.createTempFile("tmp", suffix.equals("") ? null : suffix);
		result.deleteOnExit();
		copyResourceTo(cls, resourceFilename, result);
		return result;
	}

	private static void copyResourceTo(Class<?> cls, String resourceFilename, File result) throws IOException,
			FileNotFoundException {
		try (InputStream in = cls.getResourceAsStream(resourceFilename);
				OutputStream out = new FileOutputStream(result)) {
			IOUtils.copy(in, out);
		}
	}
}
