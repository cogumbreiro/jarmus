

import java.util.concurrent.Phaser;

import pt.ul.jarmus.JArmus;

public class PhaserSelf {

	public static void main(String[] args) throws InterruptedException {
		final Phaser barrier1 = new Phaser(1);
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					JArmus.register(barrier1);
					barrier1.awaitAdvance(0);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		});
		t1.start();
		t1.join();
	}

}
