

import java.util.concurrent.CountDownLatch;

import pt.ul.jarmus.JArmus;

public class LatchSelf {

	public static void main(String[] args) throws InterruptedException {
		final CountDownLatch barrier1 = new CountDownLatch(2);
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					JArmus.register(barrier1);
					barrier1.await();
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		});
		t1.start();
		//t1.join();
	}

}
