

import java.util.concurrent.Phaser;

import pt.ul.jarmus.JArmus;

public class SplitPhase {
	protected static final boolean condition = false;

	public static void main(String[] args) throws InterruptedException {
		final Phaser a = new Phaser(2);
		final Phaser b = new Phaser(2);
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					JArmus.register(a);
					JArmus.register(b);
					if (condition) {
						b.arrive();
					}
					a.arriveAndAwaitAdvance();
					if (condition) {
						b.awaitAdvance(0);
					} else {
						b.arriveAndAwaitAdvance();
					}
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}).start();
		JArmus.register(a);
		JArmus.register(b);
		b.arriveAndAwaitAdvance();
		a.arriveAndAwaitAdvance();
	}
}
