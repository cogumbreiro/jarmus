

import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicReference;

public class ThreadSelf {
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		final AtomicReference<Thread> cell = new AtomicReference<>();
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				try {
					cell.get().join();
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		};
		Thread t1 = new Thread(runnable);
		cell.set(t1);
		t1.start();
		t1.join();
	}
}
