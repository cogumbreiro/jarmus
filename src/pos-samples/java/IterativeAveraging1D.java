import java.util.Arrays;
import java.util.concurrent.Phaser;

import pt.ul.jarmus.JArmus;
/**
 * Iterative averaging example.
 * @author Tiago Cogumbreiro
 *
 */
public class IterativeAveraging1D {
	private static final int I = 4;
	protected static final int J = 100;
	static final int[] a = new int[I + 2];
	public static void main(String[] args) {
		final Phaser c = new Phaser(1); // "Cyclic barrier" phaser
		final Phaser b = new Phaser(1); // "Join barrier" phaser
		JArmus.register(c);
		JArmus.register(b);
		for (int _i = 1; _i <= I; _i++) {
			final int i = _i;
			c.register();
			b.register();
			new Thread() { // Spawn task i
				public void run() {
				    try {
					    JArmus.register(c);
					    JArmus.register(b);
					    for (int j = 1; j <= J; j++) {
						    final int l = a[i - 1];
						    final int r = a[i + 1];
						    c.arriveAndAwaitAdvance();
						    a[i] = (l + r) / 2;
						    c.arriveAndAwaitAdvance();
					    }
					    c.arriveAndDeregister();
					    b.arriveAndDeregister();
					} catch (Exception e) {
					    c.forceTermination(); // break the deadlock
					    b.forceTermination(); // break the deadlock
					    System.err.println("DeadlockIdentifiedException T" + i);
				    }
				}
			}.start();
		}
		try {
    		b.arriveAndAwaitAdvance();
    		handle(a);
		} catch (Exception e) {
		    c.forceTermination(); // break the deadlock
		    b.forceTermination(); // break the deadlock
		    System.err.println("DeadlockIdentifiedException T0");
		}
	}
	private static void handle(int[] array) {
		System.out.println(Arrays.toString(array));
	}
}
