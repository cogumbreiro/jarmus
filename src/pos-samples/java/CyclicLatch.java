

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

import pt.ul.jarmus.JArmus;

public class CyclicLatch {

	public static void main(String[] args) throws InterruptedException {
		final CyclicBarrier barrier1 = new CyclicBarrier(2);
		final CountDownLatch barrier2 = new CountDownLatch(2);
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					JArmus.register(barrier1);
					JArmus.register(barrier2);
					barrier1.await();
					barrier2.countDown();
					barrier2.await();
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		});
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					JArmus.register(barrier1);
					JArmus.register(barrier2);
					barrier2.countDown();
					barrier2.await();
					barrier1.await();
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		});
		t1.start();
		t2.start();
		//t1.join();
		//t2.join();
	}

}
