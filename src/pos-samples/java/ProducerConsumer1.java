

import java.util.concurrent.Phaser;

import pt.ul.jarmus.JArmus;

public class ProducerConsumer1 {
	protected static final int nz = 1000;

	public static void main(String[] args) {
		final int num_threads = 3;
		final Phaser ph[] = new Phaser[num_threads];
		for (int i = 0; i < num_threads; i++) {
			ph[i] = new Phaser(2);
		}
		for (int i = 0; i < num_threads; i++) {
			final Phaser curr = ph[i];
			final Phaser neighbour = ph[(i - 1 + num_threads) % num_threads];
			final int id = i;
			new Thread(new Runnable() {
				@Override
				public void run() {
					JArmus.register(curr);
					JArmus.register(neighbour);
					for (int k = 1; k <= nz - 2; k++) {
						step(k);
						/* we introduce the deadlock by commenting one of the 
						   boundary conditions */
						//if (id > 0) {
							neighbour.arriveAndAwaitAdvance();
						//}
						step2(k);
						if (id < num_threads - 1) {
							curr.arriveAndAwaitAdvance();
						}
					}
				}

				private void step2(int k) {
					// do nothing
				}

				private void step(int k) {
					// do nothing
				}
			}).start();
		}
	}
}
