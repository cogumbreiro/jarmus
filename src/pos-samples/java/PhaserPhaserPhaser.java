

import java.util.concurrent.Phaser;

import pt.ul.jarmus.JArmus;

public class PhaserPhaserPhaser {

	public static void main(String[] args) throws InterruptedException {
		final Phaser barrier1 = new Phaser(2);
		final Phaser barrier2 = new Phaser(2);
		final Phaser barrier3 = new Phaser(2);
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					JArmus.register(barrier1);
					JArmus.register(barrier2);
					barrier1.arriveAndAwaitAdvance();
					barrier2.arriveAndAwaitAdvance();
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		});
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					JArmus.register(barrier2);
					JArmus.register(barrier3);
					barrier2.arriveAndAwaitAdvance();
					barrier3.arriveAndAwaitAdvance();
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		});
		Thread t3 = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					JArmus.register(barrier3);
					JArmus.register(barrier1);
					barrier3.arriveAndAwaitAdvance();
					barrier1.arriveAndAwaitAdvance();
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		});
		t1.start();
		t2.start();
		t3.start();
		t1.join();
		t2.join();
		t3.join();
	}

}
