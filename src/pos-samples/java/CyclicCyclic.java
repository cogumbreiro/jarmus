

import java.util.concurrent.CyclicBarrier;

import pt.ul.jarmus.JArmus;

public class CyclicCyclic {

	public static void main(String[] args) throws InterruptedException {
		final CyclicBarrier barrier1 = new CyclicBarrier(2);
		final CyclicBarrier barrier2 = new CyclicBarrier(2);
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					JArmus.register(barrier2);
					JArmus.register(barrier1);
					barrier2.await();
					barrier1.await();
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		});
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				JArmus.register(barrier2);
				JArmus.register(barrier1);
				try {
					barrier1.await();
					barrier2.await();
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		});
		t1.start();
		t2.start();
		t1.join();
		t2.join();
	}

}
