

import java.util.concurrent.Phaser;

import pt.ul.jarmus.JArmus;

public class DynamicMembership {
	public static void main(String[] args) {
		final Phaser a = new Phaser(2);
		final Phaser b = new Phaser(2);
		new Thread(new Runnable() {
			@Override
			public void run() {
				JArmus.register(a);
				JArmus.register(b);
				a.arriveAndAwaitAdvance();
			}
		}).start();
		JArmus.register(a);
		JArmus.register(b);
		b.arriveAndAwaitAdvance();
	}
}
