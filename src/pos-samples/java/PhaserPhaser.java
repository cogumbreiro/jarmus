

import java.util.concurrent.Phaser;

import pt.ul.jarmus.JArmus;

public class PhaserPhaser {

	public static void main(String[] args) throws InterruptedException {
		final Phaser barrier1 = new Phaser(2);
		final Phaser barrier2 = new Phaser(2);
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					JArmus.register(barrier1);
					JArmus.register(barrier2);
					barrier1.arriveAndAwaitAdvance();
					barrier2.arriveAndAwaitAdvance();
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		});
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					JArmus.register(barrier1);
					JArmus.register(barrier2);
					barrier2.arriveAndAwaitAdvance();
					barrier1.arriveAndAwaitAdvance();
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		});
		t1.start();
		t2.start();
		t1.join();
		t2.join();
	}

}
